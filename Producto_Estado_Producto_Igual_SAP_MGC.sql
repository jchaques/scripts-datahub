SELECT 'PRODUCTO' AS INFORME_SAP,
      
       LTRIM(A.MATNR,0) AS MERCA,
       A.MSTAE AS ESTADO_PRODUCTO_SAP,
       (CASE WHEN NOT EXISTS (SELECT 1 FROM ADM_DATAHUB.B_SAP_MARM_MVIEW MARM WHERE MARM.M_MERCA_BASE = MARA.M_MERCA AND MARM.M_MERCA_UOM IS NOT NULL) 
                              THEN DECODE(BASE.PR_ESTADO,'F','A',BASE.PR_ESTADO)
                              ELSE (CASE WHEN BASE.PR_ESTADO = 'A' 
                                        THEN 'A' 
                                        ELSE (SELECT MIN(DECODE(PACK.PR_ESTADO,'F','A',PACK.PR_ESTADO)) FROM ADM_DATAHUB.B_SAP_MARM_MVIEW MARM, ADM_DATAHUB.MGC_PRODUCTOS_MVIEW PACK
                                               WHERE MARM.M_MERCA_BASE = MARA.M_MERCA AND MARM.M_MERCA_UOM IS NOT NULL AND MARM.M_MERCA_UOM = PACK.PR_COD_PRODUCTO
                                               GROUP BY MARM.M_MERCA_BASE)
                                    END)
        END) AS ESTADO_PRODUCTO_LEGACY,

      'P33 - Estado del producto distinto entre SAP y MGC (VENTA - NO Implantados)' AS TXT_ERROR
           
FROM ADM_DATAHUB.MARA A, ADM_DATAHUB.MGC_PRODUCTOS_MVIEW BASE, ADM_DATAHUB.B_SAP_MARA_MVIEW MARA 
WHERE BASE.PR_COD_PRODUCTO = LTRIM(A.MATNR,0)
AND LTRIM(A.MATNR,0) = MARA.M_MERCA AND MARA.M_IMPLANTADO_REING = 'N'

AND A.MSTAE <> (CASE WHEN NOT EXISTS ( SELECT 1 
                                       FROM ADM_DATAHUB.B_SAP_MARM_MVIEW MARM 
                                       WHERE MARM.M_MERCA_BASE = MARA.M_MERCA 
                                       AND MARM.M_MERCA_UOM IS NOT NULL )        THEN DECODE(BASE.PR_ESTADO,'F','A',BASE.PR_ESTADO)
                              
                              
                                    ELSE (CASE WHEN BASE.PR_ESTADO = 'A' 
                                        THEN 'A' 
                                        ELSE (SELECT MIN(DECODE(PACK.PR_ESTADO,'F','A',PACK.PR_ESTADO)) FROM ADM_DATAHUB.B_SAP_MARM_MVIEW MARM, ADM_DATAHUB.MGC_PRODUCTOS_MVIEW PACK
                                               WHERE MARM.M_MERCA_BASE = MARA.M_MERCA AND MARM.M_MERCA_UOM IS NOT NULL AND MARM.M_MERCA_UOM = PACK.PR_COD_PRODUCTO
                                               GROUP BY MARM.M_MERCA_BASE)
                                    END)
                         END)

-- Comprobamos que no esté en variaciones pendientes con un cambio de estado a nivel de producto
AND NOT EXISTS (SELECT 1 FROM MGC_VARIACIONES_PDTE_MVIEW WHERE MARA.M_MERCA = VAR_COD_PRODUCTO AND VAR_VALOR IN ('AN','NA'))

-- COMPROBAMOS SI ESTA N EN MGC Y SIN TIENDAS QUE  EXISTE EN S_SURTIDO_FECHAS CON TIENDAS
AND BASE.PR_ESTADO <> 'N' AND BASE.PR_COD_PRODUCTO NOT IN (SELECT CP_COD_PRODUCTO FROM MGC_CENTRO_PRODUCTO_MVIEW WHERE CP_ESTADO <> 'N') AND BASE.PR_COD_PRODUCTO NOT IN
(SELECT COD_N_MERCA FROM S_SURTIDO_FECHAS_MVIEW WHERE FEC_D_INICIO <= SYSDATE)

--Filtramos por el andamio                        
AND EXISTS (SELECT 1 
            FROM ADM_DATAHUB.S_ARTICULO_MERCA_MVIEW S
            WHERE FEC_D_ALTA < '01/01/2021' 
            AND TO_CHAR(S.M_MERCA_UOM) = LTRIM(A.MATNR,0));