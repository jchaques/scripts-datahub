SELECT 'SURTIDO' AS INFORME_SAP,
        LTRIM (A.ARTNR, 0) AS MERCA, 
        LTRIM (A.FILIA) AS CENTRO_SAP,
        CEN.NUMERO_CENTRO AS CENTRO_LEGACY,
        A.DATAB AS FECHA_INICIO_SAP,
        A.DATBI AS FECHA_FIN_SAP,
        CEN.FECHA_APERTURA AS FECHA_APERTURA,
        'SA1 - Existe relación Producto-Almacén en SAP pero no en MGC (VENTA - NO Implantados)' AS TXT_ERROR
FROM ADM_DATAHUB.WLK1 A, adm_datahub.dt_d_centro_ext_v_mview CEN
WHERE CEN.COD_V_SITE_SAP = ltrim(A.FILIA, 0) 
AND CEN.TIPO_CENTRO = 'AL'
AND (A.DATAB >= SYSDATE OR A.DATBI BETWEEN SYSDATE AND SYSDATE+7) -- Para ver cambios a futuro a nivel de surtido

 

AND NOT EXISTS ( SELECT 1 
             FROM MGC_SERVICIO_EXTERNO_MVIEW SE
             WHERE SE.SE_cod_producto = Ltrim(A.artnr, 0) 
             AND SE.SE_COD_CENTRO = CEN.NUMERO_CENTRO )
                       


/* Filtramos por el andamio */
AND EXISTS ( SELECT 1 
             FROM   adm_datahub.s_articulo_merca_mview S 
             WHERE  S.fec_d_alta < '01/01/2021' 
             AND Ltrim(A.artnr, 0) = S.m_merca_uom );