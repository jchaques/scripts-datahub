            SELECT 'PRODUCTO' AS INFORME_SAP,
        PR.PR_COD_PRODUCTO AS MERCA_MGC ,
        PR.PR_COD_ETIQ AS TIPO_ETIQUETA_MGC,
        'P31 - Un producto tiene el campo tipo etiqueta en MGC no existe en SAP (VENTA)' AS TXT_ERROR
FROM ADM_DATAHUB.MGC_PRODUCTOS_MVIEW PR        

WHERE PR.PR_COD_ETIQ NOT IN (SELECT SUBSTR(ADDIMAT, 14, 2) FROM ADM_DATAHUB.WTADDI)

AND EXISTS (SELECT 1 
            FROM ADM_DATAHUB.S_ARTICULO_MERCA_MVIEW S
            WHERE S.FEC_D_ALTA < '01/01/2021' 
            AND to_char(S.M_MERCA_UOM) = PR.PR_COD_PRODUCTO);