SELECT 'SURTIDO' AS INFORME_SAP,
       CP.CP_COD_PRODUCTO AS MERCA, 
        CEN.COD_V_SITE_SAP AS CENTRO_SAP,
                 CP.CP_COD_CENTRO AS CENTRO_LEGACY,
                 CP.CP_FECHA_ALTA AS FECHA_ALTA_MGC,
                 'S5 - Existe relaci�n Centro-Producto en MGC pero NO en SAP (VENTA - NO Implantados)' AS TXT_ERROR
FROM ADM_DATAHUB.MGC_CENTRO_PRODUCTO_MVIEW CP, adm_datahub.dt_d_centro_ext_v_mview CEN, ADM_DATAHUB.MGC_CENTROS_MVIEW CE
WHERE CEN.NUMERO_CENTRO = CP.CP_COD_CENTRO
AND CP.CP_COD_CENTRO = CEN.NUMERO_CENTRO AND CEN.TIPO_CENTRO = 'TI'
AND CEN.FECHA_APERTURA < '31/12/2019'
AND (CEN.FECHA_CIERRE_DEFINITIVO IS NULL OR CEN.FECHA_CIERRE_DEFINITIVO > SYSDATE)
AND CEN.NUMERO_CENTRO = CE.CE_COD_CENTRO AND CE.CE_SURTIDO_VALIDADO = 'S' AND CE.CE_TIENDA_ABIERTA = 'S'
AND (CP.CP_FECHA_ALTA >= SYSDATE or CP.CP_FEC_CAMBIO_ESTADO >= sysdate)
   
AND NOT EXISTS (SELECT 1 
                FROM ADM_DATAHUB.wlk1 A 
                WHERE cp.cp_cod_producto = Ltrim(A.artnr, 0)
                AND Ltrim(A.filia, 0) = cen.cod_v_site_sap)
                
-- QUE NO EXISTA EN VARIACIONES PENDIENTES A FUTURO 

AND NOT EXISTS (SELECT 1 
FROM ADM_DATAHUB.MGC_VARIACIONES_PDTE_MVIEW PDTE 
WHERE PDTE.VAR_COD_PRODUCTO = CP.CP_COD_PRODUCTO AND PDTE.VAR_COD_CENTRO = CP.CP_COD_CENTRO)

-- QUE NO EXISTA EN S_SURTIDO_FECHAS
AND NOT EXISTS (SELECT 1 
FROM ADM_DATAHUB.S_SURTIDO_FECHAS_MVIEW SRT 
WHERE SRT.COD_N_MERCA = CP.CP_COD_PRODUCTO AND SRT.COD_N_MERCA = CP.CP_COD_CENTRO)

/* FILTRAMOS POR EL ANDAMIO */

AND EXISTS (SELECT 1 
            FROM ADM_DATAHUB.S_ARTICULO_MERCA_MVIEW S 
            WHERE S.FEC_D_ALTA < '01/01/2021' 
            AND S.M_MERCA_UOM = CP.CP_COD_PRODUCTO); 