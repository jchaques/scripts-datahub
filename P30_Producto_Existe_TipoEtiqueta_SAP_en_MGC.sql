SELECT  'PRODUCTO' AS INFORME_SAP,
        Ltrim (W.MATNR, 0) AS MERCA_SAP,
        SUBSTR(W.ADDIMAT, 14, 2) AS TIPO_ETIQUETA_SAP,
        'P30 - El tipo de etiqueta de SAP NO existe en MGC' AS TXT_ERROR
FROM ADM_DATAHUB.WTADDI W, ADM_DATAHUB.MARA A--, ADM_DATAHUB.B_SAP_MARA_MVIEW MARA
WHERE SUBSTR(W.ADDIMAT, 14, 2) IS NOT NULL  -- para que sólo SE MUESTREN los mercas con etiqueta asignada.
AND Ltrim (A.MATNR,0) = Ltrim (W.MATNR, 0)

AND SUBSTR(W.ADDIMAT, 14, 2) NOT IN (SELECT PR_COD_ETIQ FROM MGC_PRODUCTOS_MVIEW)
             
AND EXISTS (SELECT 1 
            FROM ADM_DATAHUB.S_ARTICULO_MERCA_MVIEW S
            WHERE S.FEC_D_ALTA < '01/01/2021' 
            AND to_char(S.M_MERCA_UOM) = LTRIM(W.MATNR, 0))
            
/* Excluimos los que esten en variaciones pendientes con subtipo de comando 6 */
AND NOT EXISTS ( SELECT 1 
                 FROM MGC_VARIACIONES_PDTE_MVIEW 
                 WHERE Ltrim(A.MATNR, 0) = VAR_COD_PRODUCTO 
                 AND VAR_SUBTIPO_COMANDO = 6);
