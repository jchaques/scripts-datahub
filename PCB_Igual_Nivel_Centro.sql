SELECT 'PCB' AS INFORME_SAP,
           Ltrim(A.MATNR, 0)  AS MERCA, 
           A.LIFNR AS PROVEEDOR_SAP,
           T1.COD_N_PROVEEDOR AS PROVEEDOR_LEGACY,
           t2.kbetr/t2.kpein as PCB_SAP,
           SEXT.SE_PCB AS PCB_MGC,
           --T2.KNUMH AS KNUMH,
           T3.COD_V_SITE_SAP AS CENTRO_SAP,
           T3.NUMERO_CENTRO AS CENTRO_LEGACY,
           T1.COD_N_EMPRESA AS ID_EMPRESA,
           --b.ERDAT AS CREACION_CONDICION,
          -- sa.fec_d_alta as fecha_migracion,
           A.datab AS FECHA_INICIO_PCB,
           A.datbi AS FECHA_FIN_PCB,
           'PCB de SAP a Nivel Centro es IGUAL en MGC' AS TXT_ERROR           
FROM
    ADM_DATAHUB.a017 A,
    dt_s_trave_prov_view_mview T1,
    konp T2,
    dt_d_centro_ext_v_mview T3,
    konh b,
    S_ARTICULO_MERCA_MVIEW SA
    
LEFT JOIN MGC_SERVICIO_EXTERNO_MVIEW SEXT ON (SEXT.SE_COD_PRODUCTO = SA.M_MERCA_UOM)
    
WHERE

    A.LIFNR = T1.cod_v_proveedor_sap
    AND t2.knumh = a.knumh
    AND SA.M_MERCA_UOM = Ltrim(A.MATNR, 0)
    and A.WERKS = t3.cod_v_site_sap
    and a.datbi >= sysdate
    --and (A.datab = sysdate or a.datab = sysdate+1) - quita planificados
    and a.datab <= sysdate-1 -- coge los de fecha pasada y hoy
    and b.knumh = a.knumh
    and b.ERDAT > SA.FEC_D_ALTA -- Excluimos los que NO se han cambiado post migracion, solo los modificados que estan en convivencia
    AND T1.COD_N_EMPRESA = LTRIM(T3.EMPRESA,0)
    AND T3.NUMERO_CENTRO = sext.se_cod_centro  -- join con servicio externo
    AND SEXT.SE_COD_PROVEEDOR = T1.COD_N_PROVEEDOR -- join con servicio externo
/* Que no exista en MGC a nivel de centro*/

AND NOT EXISTS
    (

    SELECT 1 
     FROM ADM_DATAHUB.MGC_SERVICIO_EXTERNO_MVIEW SE 
     WHERE SE.SE_COD_PRODUCTO = Ltrim(A.MATNR, 0) AND
     se.se_pcb = t2.kbetr/t2.kpein -- divido por la unidad
     AND SE.SE_COD_CENTRO = T3.NUMERO_CENTRO
     AND SE.SE_COD_PROVEEDOR = T1.COD_N_PROVEEDOR
     )


/* Filtramos por el andamio */

AND EXISTS ( SELECT 1 
             FROM ADM_DATAHUB.S_ARTICULO_MERCA_MVIEW S 
             WHERE S.FEC_D_ALTA < '01/01/2021' 
             AND S.M_MERCA_UOM = Ltrim(A.MATNR,0) );
             