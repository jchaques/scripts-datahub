SELECT 'PRODUCTO' AS INFORME_SAP,
       PR.PR_COD_PRODUCTO AS MERCA,
       PR.PR_ESTADO AS ESTADO_MGC,
       PR.PR_FEC_ALTA_SISTEMA AS FECHA_ALTA_MGC,
       'P1 - Existe Producto en MGC pero no en SAP (VENTA)' AS TXT_ERROR
FROM ADM_DATAHUB.MGC_PRODUCTOS_MVIEW PR
WHERE NOT EXISTS (SELECT 1 
                  FROM ADM_DATAHUB.MARA M
                  WHERE TO_CHAR(PR.PR_COD_PRODUCTO) = LTRIM(M.MATNR,'0'))

/* Excluismos los productos que se excluyen en las migraciones */
AND NOT EXISTS (SELECT 1 
                FROM ADM_DATAHUB.B_SAP_MERCA_EXCLUSION_MVIEW
                WHERE M_MERCA = PR_COD_PRODUCTO)
                
/* Filtramos por el andamio */               
AND EXISTS (SELECT 1 
            FROM ADM_DATAHUB.S_ARTICULO_MERCA_MVIEW S
            WHERE S.FEC_D_ALTA < '01/01/2021' 
            AND S.M_MERCA_UOM = PR.PR_COD_PRODUCTO);