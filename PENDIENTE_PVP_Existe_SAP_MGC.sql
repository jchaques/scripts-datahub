-- Existe PVP en SAP pero NO en MGC (VENTA - NO Implantados)
	
	SELECT 'PVP' AS INFORME_SAP,
		   Ltrim(A.matnr, 0) AS MERCA, 
		   Ltrim(A.knumh, 0) AS CENTRO_SAP,
           'PVP1 - Existe PVP en SAP pero NO en MGC (VENTA - NO Implantados)' AS TXT_ERROR
	  FROM ADM_DATAHUB.A073 A
	 WHERE NOT EXISTS (SELECT 1 FROM ADM_DATAHUB.MGC_PROD_TARIFAS_MVIEW PT WHERE ltrim(A.matnr, 0) = to_char(PT.PT_COD_PRODUCTO))
	   AND EXISTS (SELECT 1 FROM ADM_DATAHUB.S_ARTICULO_MERCA_MVIEW S WHERE S.FEC_D_ALTA < '01/01/2021' AND to_char(S.M_MERCA_UOM) = ltrim(A.matnr, 0));
	   
----------------------------------------

-- El PVP a nivel GENERAL de SAP es igual al de Legacy

	   SELECT distinct 'PVP' AS INFORME_SAP,
		   Ltrim(A.matnr, 0) AS MERCA, 
		   ' Unidad medida: ' || A.VRKME || ' PVP: ' || K.KBETR AS DATO_SAP,
            A.datab AS FECHA_INICIO_GENERAL_SAP,
            A.VKORG as ORGANIZACION,
		   'El PVP a nivel GENERAL de SAP es igual al de Legacy' AS TXT_ERROR
	  FROM ADM_DATAHUB.A073 A, ADM_DATAHUB.KONP K
	 WHERE A.KNUMH = K.knumh and A.datab <= SYSDATE and (A.datbi >= SYSDATE or A.datbi = '31/12/9999')
     
 
/* Que no este a nivel de centro o de pricelist con fecha vigente */

--  quitar centros a nivel de site que salgan en la a071, en el werks
AND NOT EXISTS (SELECT 1 FROM ADM_DATAHUB.a071 C, KONP KO WHERE A.VKORG = C.VKORG AND ltrim(C.matnr, 0) = LTRIM(A.MATNR,0) AND C.datab > A.DATAB and (C.datbi <= A.datbi or C.datbi = '31/12/9999'))-- NIVEL CENTRO

--  quitar centros a nivel de pricelist que salgan en la a155, en el werks  
AND NOT EXISTS (SELECT 1 FROM ADM_DATAHUB.A155 B, KONP KO WHERE A.VKORG = B.VKORG AND ltrim(B.matnr, 0) = LTRIM(A.MATNR,0) AND B.datab > A.DATAB and (B.datbi <= A.datbi or B.datbi = '31/12/9999')) -- NIVEL PRICELIST


/* Que no exista en TRART a nivel general */
 AND NOT EXISTS (SELECT 1 FROM ADM_DATAHUB.B_SAP_PVP_ARTICLE_MVIEW PVP_A
                    WHERE (PVP_A.M_MERCA = Ltrim(A.matnr, 0) AND PVP_A.UNIDAD_MEDIDA = A.VRKME AND PVP_A.M_MERCA_UOM IS NULL AND PVP_A.VALOR_PVP = K.KBETR) 
                       OR (PVP_A.M_MERCA_UOM = Ltrim(A.matnr, 0) AND PVP_A.UNIDAD_MEDIDA = A.VRKME AND PVP_A.M_MERCA_UOM IS NOT NULL AND PVP_A.VALOR_PVP = K.KBETR))
            
AND 
(
/* Exista ese PVP en MGC */
-- Ver centro_producto y prod_tarifas solo los centros que no hemos excluido
    EXISTS (SELECT 1 FROM ADM_DATAHUB.MGC_PROD_TARIFAS_MVIEW PTM WHERE PTM.PT_COD_PRODUCTO = Ltrim(A.matnr, 0) AND K.KBETR = PTM.PT_PVP)
    OR
    
/* Exista ese PVP en Variaciones Pendientes con esa Fecha Valor */
-- Solo comprobara los centros restantes , a nivel general, solo los centros que no hemos excluido
    EXISTS (SELECT 1 FROM ADM_DATAHUB.MGC_VARIACIONES_PDTE_MVIEW VAR WHERE LTRIM(A.MATNR,0) = VAR.VAR_COD_PRODUCTO AND VAR.VAR_SUBTIPO_COMANDO = 14 AND VAR.VAR_TIPO_COMANDO = 1 AND VAR.VAR_FEC_VALOR = A.DATAB)
)

/* Que exista en el andamio */
AND EXISTS (SELECT 1 FROM ADM_DATAHUB.S_ARTICULO_MERCA_MVIEW S WHERE S.FEC_D_ALTA < '01/01/2021' AND TO_CHAR(S.M_MERCA_UOM) = Ltrim(A.matnr, 0));


------------------------------------------------

--El PVP a nivel de CENTRO de SAP es igual al de Legacy

-- VER CENTRO_PRODUCTO CON MGC_PROD TARIFAS

SELECT distinct 'PVP' AS INFORME_SAP,
		   Ltrim(A.matnr, 0) AS MERCA, 
		   ' Unidad medida: ' || A.VRKME || ' PVP: ' || K.KBETR AS DATO_SAP,
            A.datab AS FECHA_INICIO_CENTRO_SAP,
            A.VKORG as ORGANIZACION,
		   'El PVP a nivel de CENTRO de SAP es igual al de Legacy' AS TXT_ERROR
	  FROM ADM_DATAHUB.A071 A, ADM_DATAHUB.KONP K
	 WHERE A.KNUMH = K.knumh and A.datab <= SYSDATE and (A.datbi >= SYSDATE or A.datbi = '31/12/9999')
     
 /* Que no exista en TRART a nivel de centro */
 AND NOT EXISTS (SELECT 1 FROM ADM_DATAHUB.B_SAP_PVP_ARTICLE_SITE_MVIEW PVP_A
                    WHERE (PVP_A.M_MERCA = Ltrim(A.matnr, 0) AND PVP_A.UNIDAD_MEDIDA = A.VRKME AND PVP_A.M_MERCA_UOM IS NULL AND PVP_A.VALOR_PVP = K.KBETR) 
                       OR (PVP_A.M_MERCA_UOM = Ltrim(A.matnr, 0) AND PVP_A.UNIDAD_MEDIDA = A.VRKME AND PVP_A.M_MERCA_UOM IS NOT NULL AND PVP_A.VALOR_PVP = K.KBETR))
 
/* Que no est� a nivel de general o de pricelist con fecha vigente */

AND NOT EXISTS (SELECT 1 FROM ADM_DATAHUB.a073 C, KONP KO WHERE A.VKORG = C.VKORG AND ltrim(C.matnr, 0) = LTRIM(A.MATNR,0) AND C.datab > A.DATAB and (C.datbi <= A.datbi or C.datbi = '31/12/9999'))-- NIVEL GENERAL
AND NOT EXISTS (SELECT 1 FROM ADM_DATAHUB.A155 B, KONP KO WHERE A.VKORG = B.VKORG AND ltrim(B.matnr, 0) = LTRIM(A.MATNR,0) AND B.datab > A.DATAB and (B.datbi <= A.datbi or B.datbi = '31/12/9999')) -- NIVEL PRICELIST
                  

AND 
(
/* Exista ese PVP en MGC */
    EXISTS (SELECT 1 FROM ADM_DATAHUB.MGC_PROD_TARIFAS_MVIEW PTM WHERE PTM.PT_COD_PRODUCTO = Ltrim(A.matnr, 0) AND K.KBETR = PTM.PT_PVP)
    OR
/* Exista ese PVP en Variaciones Pendientes con esa Fecha Valor */
    EXISTS (SELECT 1 FROM ADM_DATAHUB.MGC_VARIACIONES_PDTE_MVIEW VAR WHERE LTRIM(A.MATNR,0) = VAR.VAR_COD_PRODUCTO AND VAR.VAR_SUBTIPO_COMANDO = 14 AND VAR.VAR_TIPO_COMANDO = 1 AND VAR.VAR_FEC_VALOR = A.DATAB)
)

/* Que exista en el andamio */
AND EXISTS (SELECT 1 FROM ADM_DATAHUB.S_ARTICULO_MERCA_MVIEW S WHERE S.FEC_D_ALTA < '01/01/2021' AND TO_CHAR(S.M_MERCA_UOM) = Ltrim(A.matnr, 0));

--------------------------------------

-- El PVP a nivel de PRICELIST de SAP es igual al de Legacy

-- VER CENTRO PRODUCTO , PROD TARIFAS Y CENTROS POR AGRUPACION PARA SABER EL PVP DE LA TARIFA/PL

SELECT distinct 'PVP' AS INFORME_SAP,
		   Ltrim(A.matnr, 0) AS MERCA, 
		   ' Unidad medida: ' || A.VRKME || ' PVP: ' || K.KBETR AS DATO_SAP,
            A.datab AS FECHA_INICIO_PRICELIST_SAP,
            --A.VKORG as ORGANIZACION,
		   'El PVP a nivel de PRICELIST de SAP es igual al de Legacy' AS TXT_ERROR
	  FROM ADM_DATAHUB.A155 A, ADM_DATAHUB.KONP K
	 WHERE A.KNUMH = K.knumh and A.datab <= SYSDATE and (A.datbi >= SYSDATE or A.datbi = '31/12/9999')
     
 /* Que no exista en TRART a nivel de PRICELIST */
 AND NOT EXISTS (SELECT 1 FROM ADM_DATAHUB.B_SAP_PVP_ART_PRICEL_MVIEW PVP_A
                    WHERE (PVP_A.M_MERCA = Ltrim(A.matnr, 0) AND PVP_A.UNIDAD_MEDIDA = A.VRKME AND PVP_A.M_MERCA_UOM IS NULL AND PVP_A.VALOR_PVP = K.KBETR) 
                       OR (PVP_A.M_MERCA_UOM = Ltrim(A.matnr, 0) AND PVP_A.UNIDAD_MEDIDA = A.VRKME AND PVP_A.M_MERCA_UOM IS NOT NULL AND PVP_A.VALOR_PVP = K.KBETR))
 
/* Que no esta a nivel de centro con fecha vigente */

--AND NOT EXISTS (SELECT 1 FROM ADM_DATAHUB.a073 C, KONP KO WHERE A.VKORG = C.VKORG AND ltrim(C.matnr, 0) = LTRIM(A.MATNR,0) AND C.datab > A.DATAB and (C.datbi <= A.datbi or C.datbi = '31/12/9999'))-- NIVEL GENERAL
AND NOT EXISTS (SELECT 1 FROM ADM_DATAHUB.A071 B, KONP KO WHERE A.VKORG = B.VKORG AND ltrim(B.matnr, 0) = LTRIM(A.MATNR,0) AND B.datab > A.DATAB and (B.datbi <= A.datbi or B.datbi = '31/12/9999')) -- NIVEL CENTRO
                  

AND 
(
/* Exista ese PVP en MGC */
    EXISTS (SELECT 1 FROM ADM_DATAHUB.MGC_PROD_TARIFAS_MVIEW PTM WHERE PTM.PT_COD_PRODUCTO = Ltrim(A.matnr, 0) AND K.KBETR = PTM.PT_PVP)
    OR
/* Exista ese PVP en Variaciones Pendientes con esa Fecha Valor */
    EXISTS (SELECT 1 FROM ADM_DATAHUB.MGC_VARIACIONES_PDTE_MVIEW VAR WHERE LTRIM(A.MATNR,0) = VAR.VAR_COD_PRODUCTO AND VAR.VAR_SUBTIPO_COMANDO = 14 AND VAR.VAR_TIPO_COMANDO = 1 AND VAR.VAR_FEC_VALOR = A.DATAB)
)

/* Que exista en el andamio */
AND EXISTS (SELECT 1 FROM ADM_DATAHUB.S_ARTICULO_MERCA_MVIEW S WHERE S.FEC_D_ALTA < '01/01/2021' AND TO_CHAR(S.M_MERCA_UOM) = Ltrim(A.matnr, 0));