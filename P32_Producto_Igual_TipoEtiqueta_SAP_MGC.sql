SELECT  'PRODUCTO' AS INFORME_SAP,
        Ltrim (W.MATNR, 0) AS MERCA ,
        SUBSTR(W.ADDIMAT, 14, 2) AS TIPO_ETIQUETA_SAP,
        
        'P32 - Un producto tiene el campo tipo etiqueta igual en SAP que en MGC' AS TXT_ERROR
FROM ADM_DATAHUB.WTADDI W, ADM_DATAHUB.MARA A--, ADM_DATAHUB.B_SAP_MARA_MVIEW MARA
WHERE SUBSTR(W.ADDIMAT, 14, 2) IS NOT NULL  -- para que sólo SE MUESTREN los mercas con etiqueta asignada.
AND Ltrim (A.MATNR,0) = Ltrim (W.MATNR, 0)
 
-- Miramos en MGC

AND NOT EXISTS ( SELECT 1
             FROM ADM_DATAHUB.MGC_PRODUCTOS_MVIEW PR
             WHERE TO_CHAR(PR.PR_COD_PRODUCTO) = LTRIM(W.MATNR,0)
             AND PR.PR_COD_ETIQ = SUBSTR(W.ADDIMAT, 14, 2) )
             
-- Filtramos por el Andamio

AND EXISTS (SELECT 1 
            FROM ADM_DATAHUB.S_ARTICULO_MERCA_MVIEW S
            WHERE S.FEC_D_ALTA < '01/01/2021' 
            AND to_char(S.M_MERCA_UOM) = LTRIM(W.MATNR, 0));