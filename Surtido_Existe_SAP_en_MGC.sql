SELECT 'SURTIDO' AS INFORME_SAP, 
       Ltrim(A.artnr, 0) AS MERCA, 
       Ltrim(A.filia, 0) AS CENTRO_SAP,
       CEN.NUMERO_CENTRO AS CENTRO_LEGACY,
       A.DATAB AS FECHA_INICIO_SAP,
       A.DATBI AS FECHA_FIN_SAP,
'S1 - Existe relacion Centro-Producto en SAP pero NO en MGC (VENTA - NO Implantados)' 
                  AS TXT_ERROR 
FROM   adm_datahub.wlk1 A, adm_datahub.dt_d_centro_ext_v_mview CEN
WHERE CEN.COD_V_SITE_SAP = ltrim(A.FILIA, 0) AND
NOT EXISTS (SELECT 1 FROM adm_datahub.mgc_centro_producto_mview CP WHERE CP.cp_cod_producto = Ltrim(A.artnr, 0) 
AND CP.CP_COD_CENTRO = CEN.NUMERO_CENTRO)
       AND Regexp_like(A.filia, '^[[:digit:]]*$')
            
/*Que no exista en S_SURTIDO_FECHAS */
AND NOT EXISTS (SELECT 1 FROM S_SURTIDO_FECHAS_MVIEW WHERE COD_N_MERCA = LTRIM(A.ARTNR,0) AND COD_N_CENTRO = CEN.NUMERO_CENTRO)

-- Que no exista en variaciones pendientes a futuro 
AND NOT EXISTS (SELECT 1 FROM MGC_VARIACIONES_PDTE_VIEW WHERE VAR_COD_PRODUCTO = LTRIM(A.ARTNR,0) AND VAR_COD_CENTRO = CEN.NUMERO_CENTRO)
       
/* Excluimos los arrepentimientos cuando la fecha de inicio y fin de sap es la misma y a su vez no existe en mgc_centro_producto pero si en s_surtido_fechas_h, es decir
   se hace en SAP pero no baja a MGC al arrepentirse pero si pasa a s_surtido_fechas_h */
AND A.DATAB <> A.DATBI AND NOT EXISTS (SELECT 1 FROM S_SURTIDO_FECHAS_H_MVIEW WHERE COD_N_MERCA = LTRIM(A.ARTNR,0) AND COD_N_CENTRO = CEN.NUMERO_CENTRO)

/* Filtramos por el andamio */
AND EXISTS (SELECT 1 
                   FROM   adm_datahub.s_articulo_merca_mview S 
                   WHERE  S.fec_d_alta < '01/01/2021' 
                          AND Ltrim(A.artnr, 0) = S.m_merca_uom);